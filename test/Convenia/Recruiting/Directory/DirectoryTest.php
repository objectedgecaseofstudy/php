<?php
namespace Convenia\Recruiting\Directory;

/**
 * Class to test Directory class
 *
 * @author vgomes
 */
class DirectoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function dataProviderValidDirectories()
    {
        return array(
            array(dirname(dirname(dirname(dirname(__FILE__)))) . "/Convenia"),
            array(dirname(dirname(dirname(__FILE__)))."/Recruiting"),
            array(dirname(dirname(__FILE__))."/Directory"),
            array(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data"),
        );
    }

    /**
     * @return array
     */
    public function dataProviderInvalidDirectories()
    {
        return array(
            array(dirname(dirname(dirname(__FILE__))) . "/Convenia"),
            array(dirname(dirname(dirname(__FILE__)))."/Directory"),
            array(dirname(dirname(__FILE__))."/Recruiting"),
            array(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/NuData"),
        );
    }

    /**
     * @return array
     */
    public function dataProviderValidDirectoriesList()
    {
        return array(array(
            array(
                'sDirectory' => dirname(dirname(dirname(dirname(__FILE__)))) . "/Convenia",
                'aDirectoryList' =>  array('Recruiting'),
                ),

            array(
                'sDirectory' => dirname(dirname(dirname(__FILE__)))."/Recruiting",
                'aDirectoryList' =>  array('Db','Directory','Service','Validator'),
                ),
            array(
                'sDirectory' => dirname(dirname(__FILE__))."/Directory",
                'aDirectoryList' =>  array('DirectoryTest.php'),
                ),
        ));
    }

     /**
     * @return array
     */
    public function dataProviderInvalidDirectoriesList()
    {
        return array(array(
            array(
                'sDirectory' => dirname(dirname(dirname(dirname(__FILE__)))) . "/Convenia",
                'aDirectoryList' =>  array('Db', 'Service','Directory','Validator'),
                ),

            array(
                'sDirectory' => dirname(dirname(dirname(__FILE__)))."/Recruiting",
                'aDirectoryList' =>  array('Recruiting'),
                ),
            array(
                'sDirectory' => dirname(dirname(__FILE__))."/Directory",
                'aDirectoryList' =>  array('ServiceFactoryTest.php'),
                ),
        ));
    }


    /**
     * @covers ::__construct
     * @covers ::reset
     * @covers ::validate
     * @dataProvider dataProviderValidDirectories
     * @param string $sDirectory
     */
    public function testValidDirectories($sDirectory)
    {
        $oDirectory = new Directory($sDirectory);
        $this->assertEquals($sDirectory, $oDirectory->__toString());
    }

    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::Validate
     * @dataProvider dataProviderInvalidDirectories
     * @expectedException \InvalidArgumentException
     * @param string $sDirectory
     */
    public function testInvalidURLs($sDirectory)
    {
        $oDirectory = new Directory($sDirectory);
        $this->assertTrue(false, $sDirectory);
    }

    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::Validate
     * @covers ::getAll
     * @dataProvider dataProviderValidDirectoriesList
     * @param mixed[] $aDirectoryList
     */
    public function testValidListOfAllFiles($aDirectoryList)
    {
        
        $oDirectory = new Directory($aDirectoryList['sDirectory']);
        $this->assertEquals($aDirectoryList['aDirectoryList'], $oDirectory->getAll($oDirectory->__toString()));
    }

    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::Validate
     * @covers ::getAll
     * @dataProvider dataProviderInvalidDirectoriesList
     * @param mixed[] $aInvalidDirectoryList
     */
    public function testInvalidListOfAllFiles($aInvalidDirectoryList)
    {
        $oDirectory = new Directory($aInvalidDirectoryList['sDirectory']);
        $this->assertNotEquals($aInvalidDirectoryList['aDirectoryList'], $oDirectory->getAll($oDirectory->__toString()));
    }
}
