<?php
namespace Convenia\Recruiting\Validator;

/**
 * Represents a Json PDO in the result.
 *
 * @author vgomes
 */
class UrlTest extends \PHPUnit_Framework_TestCase
{

    // ----------------------------------------------------------------------------------------------------------------- Constants


    /**
     * This pattern matches all the test cases from the challenge and was the base for the regular expression I came up
     * with. A few tweaks here and there and it would be as good as my current regular expression is; of course Unicode
     * normalization to NFC is missing (done in PHP) and IPv6 validation (done in PHP) as well.
     *
     * @see https://gist.github.com/dperini/729294
     * @var string
     */
    const DPERINI_PATTERN = '_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:/\S*)?$_iuS';


    // ----------------------------------------------------------------------------------------------------------------- Data Provider


    /**
     * @return array
     */
    public function dataProviderValidURLs()
    {
        return array(
            array("http://foo.com/blah_blah"),
            array("http://foo.com/blah_blah/"),
            array("http://foo.com/blah_blah_(wikipedia)"),
            array("http://foo.com/blah_blah_(wikipedia)_(again)"),
            array("http://www.example.com/wpstyle/?p=364"),
            array("https://www.example.com/foo/?bar=baz&inga=42&quux"),
            array("http://userid:password@example.com:8080"),
            array("http://userid:password@example.com:8080/"),
            array("http://userid@example.com"),
            array("http://userid@example.com/"),
            array("http://userid@example.com:8080"),
            array("http://userid@example.com:8080/"),
            array("http://userid:password@example.com"),
            array("http://userid:password@example.com/"),
            array("http://142.42.1.1/"),
            array("http://142.42.1.1:8080/"),
            array("http://foo.com/blah_(wikipedia)#cite-1"),
            array("http://foo.com/blah_(wikipedia)_blah#cite-1"),
            array("http://foo.com/unicode_(✪)_in_parens"),
            array("http://foo.com/(something)?after=parens"),
            array("http://code.google.com/events/#&product=browser"),
            array("http://j.mp"),
            array("http://foo.bar/?q=Test%20URL-encoded%20stuff"),
            array("http://1337.net"),
            array("http://a.b-c.de"),
            array("http://www.öbb.at/"),
            array("http://www.xn-bb-eka.at"),
            array("http://a.b-c.de/"),
            array("http://www.example.com/path?test=123"),
        );
    }

    /**
     * @return array
     */
    public function dataProviderInvalidType()
    {
        return array(
            array(null),
            array(0),
            array(0.0),
            array(""),
            array(true),
            array(false),
            array(array()),
            array((object) array()),
        );
    }

    /**
     * @return array
     */
    public function dataProviderInvalidURLs()
    {
        $c1 = chr(1);
        $c2 = chr(2);
        $c3 = chr(3);
        $c4 = chr(4);
        $c5 = chr(5);
        $c6 = chr(6);
        $c7 = chr(7);
        $c8 = chr(8);
        $c14 = chr(14);
        $c15 = chr(15);
        $c16 = chr(16);
        $c17 = chr(17);
        $c18 = chr(18);
        $c19 = chr(19);
        $c20 = chr(20);
        $c21 = chr(21);
        $c22 = chr(22);
        $c23 = chr(23);
        $c24 = chr(24);
        $c127 = chr(127);

        return array(
            array("http://"),
            array("http://."),
            array("http://.."),
            array("http://../"),
            array("http://?"),
            array("http://??"),
            array("http://??/"),
            array("http://#"),
            array("http://##"),
            array("http://##/"),
            array("http://foo.bar?q=Spaces should be encoded"),
            array("//"),
            array("//a"),
            array("///a"),
            array("///"),
            array("http:///a"),
            array("foo.com"),
            array("rdar://1234"),
            array("h://test"),
            array("http:// shouldfail.com"),
            array(":// should fail"),
            array("http://foo.bar/foo(bar)baz quux"),
            array("ftps://foo.bar/"),
            array("http://-error-.invalid/"),
            array("http://-a.b.co"),
            array("http://a.b-.co"),
            array("http://0.0.0.0"),
            array("http://10.1.1.0"),
            array("http://10.1.1.255"),
            array("http://224.1.1.1"),
            array("http://1.1.1.1.1"),
            array("http://123.123.123"),
            array("http://3628126748"),
            array("http://.www.foo.bar/"),
            array("http://www.foo.bar./"),
            array("http://.www.foo.bar./"),
            array("http://10.1.1.1"),
            array("http://203.0.113.0"),
            array("http://198.51.100.0"),
            array("http://192.0.2.0"),
            array("http://www.foo\0bar.com"),
            array("http://www.foo{$c1}bar.com"),
            array("http://www.foo{$c2}bar.com"),
            array("http://www.foo{$c3}bar.com"),
            array("http://www.foo{$c4}bar.com"),
            array("http://www.foo{$c5}bar.com"),
            array("http://www.foo{$c6}bar.com"),
            array("http://www.foo{$c7}bar.com"),
            array("http://www.foo{$c8}bar.com"),
            array("http://www.foo\tbar.com"),
            array("http://www.foo\nbar.com"),
            array("http://www.foo\vbar.com"),
            array("http://www.foo\fbar.com"),
            array("http://www.foo\rbar.com"),
            array("http://www.foo{$c14}bar.com"),
            array("http://www.foo{$c15}bar.com"),
            array("http://www.foo{$c16}bar.com"),
            array("http://www.foo{$c17}bar.com"),
            array("http://www.foo{$c18}bar.com"),
            array("http://www.foo{$c19}bar.com"),
            array("http://www.foo{$c20}bar.com"),
            array("http://www.foo{$c21}bar.com"),
            array("http://www.foo{$c22}bar.com"),
            array("http://www.foo{$c23}bar.com"),
            array("http://www.foo{$c24}bar.com"),
            array("http://www.foo{$c127}bar.com"),
            array("http://www.foo bar.com"),
            array("http://www.foo!bar.com"),
            array('http://www.foo"bar.com'),
            array("http://www.foo#bar.com"),
            array('http://www.foo$bar.com'),
            array("http://www.foo%bar.com"),
            array("http://www.foo&bar.com"),
            array("http://www.foo'bar.com"),
            array("http://www.foo(bar.com"),
            array("http://www.foo)bar.com"),
            array("http://www.foo*bar.com"),
            array("http://www.foo+bar.com"),
            array("http://www.foo,bar.com"),
            array("http://www.foo:bar.com"),
            array("http://www.foo;bar.com"),
            array("http://www.foo<bar.com"),
            array("http://www.foo=bar.com"),
            array("http://www.foo>bar.com"),
            array("http://www.foo?bar.com"),
            array("http://www.foo[bar.com"),
            array('http://www.foo\bar.com'),
            array("http://www.foo]bar.com"),
            array("http://www.foo^bar.com"),
            array("http://www.foo_bar.com"),
            array("http://www.foo`bar.com"),
            array("http://www.foo{bar.com"),
            array("http://www.foo}bar.com"),
            array("http://www.foo|bar.com"),
        );
    }

    /**
     * @covers ::__construct
     * @covers ::reset
     * @covers ::validate
     * @dataProvider dataProviderValidURLs
     * @param string $sUrl
     */
    public function testValidURLs($sUrl)
    {
        $instance = new URL($sUrl);
        $this->assertEquals($sUrl, $instance->__toString());
    }

    /**
     * @covers ::__construct
     * @covers ::reset
     * @covers ::validate
     * @dataProvider dataProviderInvalidURLs
     * @expectedException \InvalidArgumentException
     * @param string $sUrl
     */
    public function testInvalidURLs($sUrl)
    {
        $instance = new URL();
        $instance->validate($sUrl);
        $this->assertTrue(false, $sUrl);
    }



    /**
     * @covers ::__construct
     * @covers ::reset
     * @covers ::ValidateHomepage
     */
    public function testValidHomepageUrl()
    {
        $sUrl = "http://www.google.com.br";
        $oValidator = new Url($sUrl);
        $this->assertTrue(true,$oValidator->ValidateHomepage());
    }
    

}
