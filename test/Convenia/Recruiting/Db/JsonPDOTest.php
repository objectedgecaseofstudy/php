<?php
namespace Convenia\Recruiting\Db;

/**
 * Represents a Json PDO in the result.
 *
 * @author vgomes
 */
class JsonPDOTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldGetContentFromJsonDatasourceByDatasourceCityId()
    {
    	$iCityId = 15475;
        $sDirectoryPath = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
        $oJsonPDO = new JsonPDO($sDirectoryPath,$iCityId);
        $aContent  = $oJsonPDO->getContent();
        $this->assertInternalType('array',$aContent);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldNotGetContentFromJsonDatasourceByDatasourceCityId()
    {
    	$iCityId = 367092;
    	$sDirectoryPath = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
        $oJsonPDO = new JsonPDO($sDirectoryPath,$iCityId);
		$this->setExpectedException('InvalidArgumentException');
    }


    public function testShouldGetCityFromJsonDatasource()
    {
    	$iCityId = 15475;
        $sDirectoryPath = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
        $oJsonPDO = new JsonPDO($sDirectoryPath,$iCityId);
        $sCity  = $oJsonPDO->getCity();
        $this->assertEquals('Düsseldorf',$sCity);
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldNotGetCityFromJsonDatasource()
    {
        $iCityId = 32202;
        $sDirectoryPath = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
        $oJsonPDO = new JsonPDO($sDirectoryPath,$iCityId);
        $oJsonPDO->getCity();
        $this->setExpectedException('InvalidArgumentException');
    }
    
}
