<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Directory\Directory;

class PartnerServiceTest extends \PHPUnit_Framework_TestCase
{

	/**
     * @return array
     */
    public function dataProviderValidDirectories()
    {
        return array(
            array(dirname(dirname(dirname(dirname(__FILE__)))) . "/Convenia"),
            array(dirname(dirname(dirname(__FILE__)))."/Recruiting"),
            array(dirname(dirname(__FILE__))."/Directory"),
            array(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data"),
        );
    }

    /**
     * @return array
     */
    public function dataProviderValidDirectoryPathAndCityId()
    {
    	return array( 
    			array(
    			 array( 
    				'sDirectory' => dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data",
    				'iCityId' => 15475,
    			 ),
    			)
    		   );
    }

    /**
     * @return array
     */
    public function dataProviderDatasourceInvalidJsonData()
    {
    	return array(
	    	array(
	    		array(
    			'sDirectory' => dirname(dirname(dirname(dirname(__FILE__)))) . "/data",
    			'iCityId' => 32202,
    			),
	    	),
	    	array(
	    		array(
				'sDirectory' => dirname(dirname(dirname(dirname(__FILE__)))) . "/data",
    			'iCityId' => 322021,
    			),
			),
	    	array(
	    		array(
				'sDirectory' => dirname(dirname(dirname(dirname(__FILE__)))) . "/data",
    			'iCityId' => 322022,
    			),
			),
			array(
				array(
				'sDirectory' => dirname(dirname(dirname(dirname(__FILE__)))) . "/data",
    			'iCityId' => 322023,
    			),
			),
		);
    }


    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::getResultForCityId
     * @dataProvider dataProviderValidDirectoryPathAndCityId
     * @param array $sDirectoryAndCityId
     */
    public function testShouldGetResultForCityId($sDirectoryAndCityId)
    {
    	$oPartnerService = new PartnerService(new Directory($sDirectoryAndCityId['sDirectory']));
        $oPartnerServiceResult = $oPartnerService->getResultForCityId($sDirectoryAndCityId['iCityId']);
    	$this->assertInternalType('array', $oPartnerServiceResult);
    }

    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::Build
     * @covers ::BuildCompany
     * @covers ::BuildPartner
     * @covers ::BuildPrice
     * @dataProvider dataProviderDatasourceInvalidJsonData
     * @expectedException \InvalidArgumentException
     * @param array $aDatasource
     */
    public function testInvalidArrayFormat($aDatasource)
    {
    	$oPartnerService = new PartnerService(new Directory($aDatasource['sDirectory']));
        $oPartnerServiceResult = $oPartnerService->getResultForCityId($aDatasource['iCityId']);
        $this->assertTrue(false, $oPartnerServiceResult);
    }

}
