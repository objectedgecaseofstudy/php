<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Directory\Directory;
use Convenia\Recruiting\Service\PartnerService;
use Convenia\Recruiting\Entity\Company;
use Convenia\Recruiting\Entity\Partner;

class PriceOrderedCompanyServiceTest extends \PHPUnit_Framework_TestCase
{
	/**
     * @return array
     */
    public function dataProviderValidDirectoryAndCity()
    {
    	return array( 
    			array(
    			 array( 
    				'sDirectory' => dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data",
    				'sCityName' => 'Düsseldorf',
    			 ),
    			)
    		 );
    }

    /**
     * @return array
     */
    public function dataProviderInvalidDirectoryAndCity()
    {
    	return array( 
    			array(
    			 array( 
    				'sDirectory' => dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data",
    				'sCityName' => 'Munich',
    			 ),
    			)
    		 );
    }
    
    /**
     * @covers ::__construct
     * @covers ::Reset
     * @dataProvider dataProviderValidDirectoryAndCity
     * @param array $sDirectoryAndCityName
     */
    public function testConstructAndReset($sDirectoryAndCityName)
	{
        $oPriceOrderedCompanyService = new PriceOrderedCompanyService(new PartnerService(new Directory($sDirectoryAndCityName['sDirectory'])));
	    
        $oReflectionClass = new \ReflectionClass($oPriceOrderedCompanyService);
		$oReflectionProperty = $oReflectionClass->getProperty('oPartnerService');
		$oReflectionProperty->setAccessible(true);
		$this->assertEquals(new PartnerService(new Directory($sDirectoryAndCityName['sDirectory'])), $oReflectionProperty->getValue($oPriceOrderedCompanyService));
		
		$sFakeValueForPrivate = "fakeval";
		$oReflectionProperty->setValue($oReflectionProperty,$sFakeValueForPrivate);
		$oPriceOrderedCompanyService = new PriceOrderedCompanyService(new PartnerService(new Directory($sDirectoryAndCityName['sDirectory'])));
		$oReflectionClass = new \ReflectionClass($oPriceOrderedCompanyService);
		$oReflectionProperty = $oReflectionClass->getProperty('oPartnerService');
		$oReflectionProperty->setAccessible(true);
		$this->assertNotEquals($sFakeValueForPrivate, $oReflectionProperty->getValue($oPriceOrderedCompanyService));
    	
    }

    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::getCompanysForCity
     * @covers ::createSortObject
     * @covers ::createSimpleSortableObject
     * @dataProvider dataProviderValidDirectoryAndCity
     * @param array $sDirectoryAndCityName
     */
    public function testShoulGetResultForCityName($sDirectoryAndCityName)
	{
        $oPriceOrderedCompanyService = new PriceOrderedCompanyService(new PartnerService(new Directory($sDirectoryAndCityName['sDirectory'])));
	    $oPriceOrderedCompany = $oPriceOrderedCompanyService->getCompanysForCity($sDirectoryAndCityName['sCityName']);
	   	foreach ($oPriceOrderedCompany as $iCompanyKey => $oCompanyValue) {
	   		$this->assertInstanceOf('\Convenia\Recruiting\Entity\Company',$oCompanyValue);
   			foreach ($oCompanyValue->aPartners as $iPartnerKey => $oPartnerValue) {
   				$this->assertInstanceOf('\Convenia\Recruiting\Entity\Partner', $oPartnerValue);
   				foreach ($oPartnerValue->aPrices as $iPriceKey => $oPriceValue) {
	   				$this->assertInstanceOf('\Convenia\Recruiting\Entity\Price', $oPriceValue);
   				}
   			}
	   	}
	   
    }



    /**
     * @covers ::__construct
     * @covers ::Reset
     * @covers ::getResultForCityId
     * @dataProvider dataProviderInvalidDirectoryAndCity
     * @expectedException \InvalidArgumentException
     * @param array $sDirectoryAndCityName
     */
    public function testShoulNotGetResultForCityName($sDirectoryAndCityName)
	{
        $oPriceOrderedCompanyService = new PriceOrderedCompanyService(new PartnerService(new Directory($sDirectoryAndCityName['sDirectory'])));
	    $aSortableObject = $oPriceOrderedCompanyService->getCompanysForCity($sDirectoryAndCityName['sCityName']);
	    
    }


}
