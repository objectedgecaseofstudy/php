<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Directory\Directory;
use Convenia\Recruiting\Service\PartnerService;
use Convenia\Recruiting\Service\PriceOrderedCompanyService;
use Convenia\Recruiting\Service\UnorderedCompanyService;

class ServiceFactoryTest extends \PHPUnit_Framework_TestCase
{
    
    public function testShouildGetResultForCityId()
	{
		$sDirectory = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
		$iCityId = 15475;

        $oServiceFactory = new ServiceFactory(new Directory($sDirectory));
        $oServiceFactory->getResultForCityId($iCityId);
    }

    public function testShouildGetResultForCityName()
	{
		$sDirectory = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
		$sCityName = 'Düsseldorf';

        $oServiceFactory = new ServiceFactory(new Directory($sDirectory));
        $oServiceFactory->getCompanysForCity($sCityName);
    }

    public function testShouildGetResultForCityNamePriceOrdered()
	{
		$sDirectory = dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data";
		$sCityName = 'Düsseldorf';

        $oServiceFactory = new ServiceFactory(new Directory($sDirectory));
        $oServiceFactory->getPriceOrderedCompanysForCity($sCityName);
    }


}
