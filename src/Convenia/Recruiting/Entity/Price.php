<?php
namespace Convenia\Recruiting\Entity;

/**
 * Represents a single price from a search result
 * related to a single partner.
 * 
 * @author vgomes
 */
class Price
{
    
    public function __construct($sDescription, $fAmount, $oFromDate, $oToDate)
    {
        $this->sDescription = $sDescription;
        $this->fAmount = $fAmount;
        $this->oFromDate = $oFromDate;
        $this->oToDate = $oToDate;
    }
    
    /**
     * Description text for the rate/price
     * 
     * @var string
     */
    public $sDescription;

    /**
     * Price in euro
     * 
     * @var float
     */
    public $fAmount;

    /**
     * Arrival date, represented by a DateTime obj
     * which needs to be converted from a string on 
     * write of the property.
     *
     * @var \DateTime
     */
    public $oFromDate;

    /**
     * Departure date, represented by a DateTime obj
     * which needs to be converted from a string on 
     * write of the property
     *
     * @var \DateTime
     */
    public $oToDate;
}
