<?php
namespace Convenia\Recruiting\Entity;
use Convenia\Recruiting\Validator\Url;

/**
 * Represents a single partner from a search result.
 * 
 * @author vgomes
 */
class Partner
{
    
    public function __construct($sName, $sHomepage)
    {
        $oValidatorUrl = new Url($sHomepage);
        $this->sName = $sName;
        $this->sHomepage = $oValidatorUrl->__toString();
    }
    
    /**
     * Name of the partner
     * @var string
     */
    public $sName;

    /**
     * Url of the partner's homepage (root link)
     * 
     * @var string
     */
    public $sHomepage;

    /**
     * Unsorted list of prices received from the 
     * actual search query.
     * 
     * @var Price[]
     */
    public $aPrices = array();

    public function addPrice($iKey, $oPrice)
    {
        $this->aPrices[$iKey] = $oPrice;
    }
}
