<?php
namespace Convenia\Recruiting\Entity;

/**
 * Represents a single company in the result.
 *
 * @author vgomes
 */
class Company 
{
    
    public function __construct($sName, $sAdr)
    {
        $this->sName = $sName;
        $this->sAdr = $sAdr;
    }
   
    /**
     * Name of the company.
     *
     * @var string
     */
    public $sName;

    /**
     * Street adr. of the company.
     * 
     * @var string
     */
    public $sAdr;

    /**
     * Unsorted list of partners with their corresponding prices.
     * 
     * @var Partner[]
     */
    public $aPartners = array();

    
    public function addPartner($iKey, $oPartner)
    {
        $this->aPartners[$iKey] = $oPartner; 
    }
}
