<?php
namespace Convenia\Recruiting\Validator;

/**
 * Class for validate Url
 *
 * @author Vinicius Gomes marcusllgomes.com
 */
class Url 
{
    /**
     * The regular expression pattern for URL validation.
     *
     * @var string
     */
    const URL_PATTERN = '_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$_iuS';

    /**
     * The full URL.
     *
     * @var null|string
     */
    private $sUrl;

    /**
     * Construct new URL instance.
     *
     * @param string $sUrl
     */
    public function __construct($sUrl = null)
    {
        if($sUrl !== null)
        {
            $this->validate($sUrl);
        }
    }

    /**
     * Get the string representation of the URL.
     *
     * @return string
     *   The string representation of the URL.
     */
    public function __toString()
    {
        return (string) $this->sUrl;
    }

    /**
     * Reset all properties to their defaults.
     *
     * @return $this
     */
    public function reset()
    {
        $this->sUrl      = null;

        return $this;
    }


    /**
     * Validate the URL.
     *
     * The various URL parts are exported to class scope, have a look at the public properties of this class. Note that
     * changing any of the properties does not alter the URL itself which this instance represents.
     *
     * @param string $url
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function validate($sUrl)
    {
        $this->reset();

        if($sUrl === null || $sUrl === "")
        {
            throw new \InvalidArgumentException("URL cannot be empty.");
        }

        if(!is_string($sUrl) && !(is_object($sUrl) && method_exists($sUrl, "__toString")))
        {
            throw new \InvalidArgumentException("URL must be representable as string.");
        }

        if(!preg_match(static::URL_PATTERN, $sUrl))
        {
            throw new \InvalidArgumentException('Url is invalid.');
        }

        
        $this->sUrl = $sUrl;

        return $this;
    }

    /**
     * Function to simple Homepage validation, 
     * I use Jeffrey Friedl Pattern's Found on https://mathiasbynens.be/demo/url-regex
     * Thinking a while about the pattern of Homepage's Address, I've been looking for a pattern to validation
     * I believe that the standard I have chosen is what most resembles a homepage of a site Homepage.
     *
     * @param string $sUrl
     * @return string
     */
    public function ValidateHomepage()
    {
        $pattern = '@\b((ftp|https?)://[-\w]+(\.\w[-\w]*)+|(?:[a-z0-9](?:[-a-z0-9]*[a-z0-9])?\.)+(?: com\b|edu\b|biz\b|gov\b|in(?:t|fo)\b|mil\b|net\b|org\b|[a-z][a-z]\b))(\:\d+)?(/[^.!,?;"\'<>()\[\]{}\s\x7F-\xFF]*(?:[.!,?]+[^.!,?;"\'<>()\[\]{}\s\x7F-\xFF]+)*)?@iS';
        $ResultUrl = preg_match($pattern, $this->sUrl);
        return $ResultUrl;
    }



}