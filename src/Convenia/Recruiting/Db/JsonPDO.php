<?php
namespace Convenia\Recruiting\Db;

/**
 * Represents a Json PDO in the result.
 *
 * @author Vinicius Gomes marcusllgomes.com
 */
class JsonPDO 
{
    private $Datasource;
    
    public function __construct($sFilePath, $iCityId)
    {
        $sFile = $sFilePath . "/" . $iCityId.".json";
        if(!file_exists($sFile))
        {
            throw new \InvalidArgumentException(sprintf('Given filepath [%s] not exist.', $sFile));
        }


        $this->Datasource = $sFile;
    }
    
   
    /**
     * Geting array data from datasource
     * @return []
     */
    public function getContent(){
        return json_decode(file_get_contents($this->Datasource), true);
    } 

    /**
     * Geting city name from datasource content
     * @return string
     */
    public function getCity()
    {
        $content = $this->getContent();

        if(!isset($content['city']))
        {
            throw new \InvalidArgumentException(sprintf('Given key city not exists in Json: [%s].', $this->Datasource));
        }

        return $content['city'];
    }

}