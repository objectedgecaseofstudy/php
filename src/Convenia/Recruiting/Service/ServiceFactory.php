<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Service\Interfaces\CompanyServiceInterface;
use Convenia\Recruiting\Service\Interfaces\PartnerServiceInterface;
use Convenia\Recruiting\Directory\Directory;

/**
 * @author Vinicius Gomes marcusllgomes.com
 */
class ServiceFactory implements CompanyServiceInterface, PartnerServiceInterface
{

    /**
     * @var instanceof Directory
     */
    private $oDirectory;

    /**
     * @var instanceof PartnerService
     */
    private $oPartnerService;

    /**
     * @var instanceof CompanyService
     */
    private $oUnorderedCompanyService;

    /**
     * @var instanceof PriceService
     */
    private $oPriceredOrderedCompanyService;

    

    public function __construct(Directory $oDirectory)
    {
        $this->reset();
        $this->oDirectory = $oDirectory;
    }

    /**
     * Reset all properties to their defaults.
     *
     * @return $this
     */
    private function reset()
    {
        $this->oDirectory                   = null;
        $this->oPartnerService              = null;
        $this->oUnorderedCompanyService       = null;
        $this->oPriceredOrderedCompanyService = null;
        return $this;
    }

    /**
     * This method should read from a datasource (JSON in our case)
     * and return an unsorted list of Companys found in the datasource.
     * 
     * @param integer $iCityId
     *
     * @return \Convenia\Recruiting\Entity\Company[]
     */
    public function getResultForCityId($iCityId)
    {
        $this->oPartnerService = new PartnerService($this->oDirectory);
        return $this->oPartnerService->getResultForCityId($iCityId); 
    }

    /**
     * This method should get list of Companys from partner service by city name.
     * and return an unsorted list of Companys.
     * 
     * @param integer $iCityName
     *
     * @return \Convenia\Recruiting\Entity\Company[]
     */
    public function getCompanysForCity($sCityName)
    {
        $this->oUnorderedCompanyService = new UnorderedCompanyService(new PartnerService($this->oDirectory));
        return $this->oUnorderedCompanyService->getCompanysForCity($sCityName);        
        
    }

    /**
     * This method should get sorted list of Companys by city name
     * and return sorted list of Companys.
     * 
     * @param integer $sCityName
     *
     * @return \Convenia\Recruiting\Entity\Company[]
     */
    public function getPriceOrderedCompanysForCity($sCityName)
    {
        $this->oPriceredOrderedCompanyService = new PriceOrderedCompanyService(new PartnerService($this->oDirectory));
        return $this->oPriceredOrderedCompanyService->getCompanysForCity($sCityName);        
    }

}