<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Entity\Company;
use Convenia\Recruiting\Entity\Partner;
use Convenia\Recruiting\Entity\Price;
use Convenia\Recruiting\Service\Interfaces\PartnerServiceInterface;
use Convenia\Recruiting\Directory\Directory;
use Convenia\Recruiting\Db\JsonPDO;


/**
 * This class is an implementation of basic PartnerService.
 *
 * @author Vinicius Gomes marcusllgomes.com
 */
class PartnerService implements PartnerServiceInterface
{
    /**
     * Array with datasource contet
     *
     * @var array
     */
    private $aDatasorceContent = array();
    
    /**
     * Instance of Database Json PDO. 
     *
     * @var instance of JsonPDO
     */
    private $oJsonPDO;

    /**
     * Instance of Directory Object.
     *
     * @var instance of Directory
     */
    private $oDirectory;
    

    public function __construct(Directory $oDirectory)
    {
       $this->reset();
       $this->oDirectory = $oDirectory;
    }

    /**
     * Reset all properties to their defaults.
     *
     * @return $this
     */
    private function reset()
    {
        $this->oJsonPDO             = null;
        $this->aDatasorceContent    = null;
        $this->oDirectory           = null;
        return $this;
    }


    /**
     * This method should read from a datasource (JSON in our case)
     * and return an unsorted list of Companys found in the datasource.
     * 
     * @param integer $iCityId
     *
     * @return \Convenia;\Recruiting\Entity\Company[]
     * @throws \InvalidArgumentException
     */
    public function getResultForCityId($iCityId)
    {
        if(!file_exists($this->oDirectory->__toString()."/".$iCityId . ".json"))
        {
            throw new \InvalidArgumentException(sprintf('Given CityId [%s] do not found in datasource.', $iCityId));
        }
        
        $this->oJsonPDO = new JsonPDO($this->oDirectory->__toString(),$iCityId);
        
        if(!$this->oJsonPDO->getContent())
        {
            throw new \InvalidArgumentException(sprintf('Given datasource [%s] do could no convert in array.', $this->oDirectory->__toString()."/".$iCityId . ".json"));
        }
        
        $this->aDatasorceContent = $this->oJsonPDO->getContent();
        
        return $this->Build();
    }

    /**
     * This method should start Build Process to encapsulate entitys 
     * (Company) -[hasMany]-> (Partner) -[hasMany]-> (Price)
     *
     * @return \Convenia\Recruiting\Entity\Company[]
     * @throws \InvalidArgumentException
     */
    private function build()
    {
        $aContent = $this->aDatasorceContent;
        if( !isset($aContent['companies']) || !count($aContent['companies']))
        {
            throw new \InvalidArgumentException('Given datasource do not has companies.');   
        }
        foreach($aContent['companies'] as $iCompanyKey => $company)
        {
           $companies[$iCompanyKey] = $this->buildCompany($company);
        }
        return $companies;
    }

    /**
     * Method to validate Company and put then create Company Entity  
     * (Company)
     *
     * @param mixed[] Company Object Data
     *
     * @return \Convenia\Recruiting\Entity\Company[]
     * @throws \InvalidArgumentException
     */
    private function buildCompany(array $data)
    {
        if(!isset($data['name']) || empty($data['name']))
        {
            throw new \InvalidArgumentException('Not given Company name.');
        }
        
        if(!isset($data['adr']) || empty($data['adr']))
        {
            throw new \InvalidArgumentException('Not give Company address');
        }

        if( !isset($data['partners']) || !count($data['partners']))
        {
            throw new \InvalidArgumentException('Given datasource do not has partners.');   
        }
        
        $oCompany = new Company($data['name'], $data['adr']);
        foreach($data['partners'] as $iPartneKey => $partner)
        {
            $oCompany->addPartner($iPartneKey, $this->buildPartner($partner));
        }
        return $oCompany;
        
    }

    /**
     * Method to validate Patner and put then create Partner Entity  
     * (Company) -[hasMany]-> (Partner)
     *
     * @param mixed[] Partner Object Data
     *
     * @return \Convenia\Recruiting\Entity\Partner[]
     * @throws \InvalidArgumentException
     */
    private function buildPartner(array $data)
    {
        if(!isset($data['name']) || empty($data['name']))
        {
            throw new \InvalidArgumentException('Not given Patner name.');
        }
        
        if(!isset($data['url']) || empty($data['url']))
        {
            throw new \InvalidArgumentException('Not given Partner homepage address');
        }

        if( !isset($data['prices']) || !count($data['prices']))
        {
            throw new \InvalidArgumentException('Given datasource do not has prices.');   
        }

        $oPartner = new Partner($data['name'], $data['url']);
        foreach($data['prices'] as $iPriceKey => $price)
        {
            $oPartner->addPrice($iPriceKey, $this->buildPrice($price));
        }
        return $oPartner;
    }

    /**
     * Method to validate Price and put then create Price Entity  
     * (Company) -[hasMany]-> (Partner) -[hasMany]-> (Price)
     *
     * @param mixed[] Price Object Data
     *
     * @return \Convenia\Recruiting\Entity\Price[]
     * @throws \InvalidArgumentException
     */
    private function buildPrice(array $data)
    {
        
        if(!isset($data['description']) || empty($data['description']))
        {
            throw new \InvalidArgumentException('Not given Patner price description.');
        }
        
        if(!isset($data['amount']) || empty($data['amount']))
        {
            throw new \InvalidArgumentException('Not given Partner price amount');
        }

        try
        {
            $oPrice = new Price(
                $data['description'], 
                $data['amount'], 
                new \DateTime($data['from']), 
                new \DateTime($data['to'])
                );
            return $oPrice;
        }
        catch ( \Exception $e)
        {
             throw new \InvalidArgumentException('Given Price Date could not be instance of Datetime');
        }
    }
}
