<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Service\Interfaces\CompanyServiceInterface;
use Convenia\Recruiting\Service\Interfaces\PartnerServiceInterface;
use Convenia\Recruiting\Entity\Company;
use Convenia\Recruiting\Entity\Partner;
use Convenia\Recruiting\Entity\Price;

/**
 * This class is an (unfinished) example implementation of an unordered Company service.
 *
 * @author mmueller
 */
class UnorderedCompanyService implements CompanyServiceInterface
{

    /**
     * @var PartnerServiceInterface
     */
    private $oPartnerService;

    /**
     * Maps from city name to the id for the partner service.
     *  
     * @var array
     */
    private $aCityToIdMapping = array(
            "Düsseldorf" => 15475
        );

    /**
     * @param PartnerServiceInterface $oPartnerService
     */
    public function __construct(PartnerServiceInterface $oPartnerService)
    {
        $this->reset();
        $this->oPartnerService = $oPartnerService;
    }

    /**
     * Reset all properties to their defaults.
     *
     * @return $this
     */
    private function reset()
    {
        $this->oPartnerService              = null;
        return $this;
    }

    /**
     * @inherited
     */
    public function getCompanysForCity($sCityName)
    {
        if (!isset($this->aCityToIdMapping[$sCityName]))
        {
            throw new \InvalidArgumentException(sprintf('Given city name [%s] is not mapped.', $sCityName));
        }
        $iCityId = $this->aCityToIdMapping[$sCityName];
        $aPartnerResults = $this->oPartnerService->getResultForCityId($iCityId);
    }
}