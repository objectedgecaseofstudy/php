<?php
namespace Convenia\Recruiting\Service;
use Convenia\Recruiting\Service\Interfaces\CompanyServiceInterface;
use Convenia\Recruiting\Service\Interfaces\PartnerServiceInterface;
use Convenia\Recruiting\Entity\Company;
use Convenia\Recruiting\Entity\Partner;
use Convenia\Recruiting\Entity\Price;

/**
 * @author Vinicius Gomes marcusllgomes.com
 */
class PriceOrderedCompanyService implements CompanyServiceInterface
{
    /**
     * @var instanceof PartnerServiceInterface
     */
    private $oPartnerService;

    /**
     * @var array Sortable Company by Price  
     */
    private $aSimpleSortable = array();

    /**
     * Maps from city name to the id for the partner service.
     *  
     * @var array
     */
    private $aCityToIdMapping = array(
            "Düsseldorf" => 15475
        );


    /**
     * @param PartnerServiceInterface $oPartnerService
     */
    public function __construct(PartnerServiceInterface $oPartnerService)
    {
        $this->reset();
        $this->oPartnerService = $oPartnerService;
    }

    /**
     * Reset all properties to their defaults.
     *
     * @return $this
     */
    public function reset()
    {
        $this->aSimpleSortable  =   null;
        $this->oPartnerService  =   null;
        return $this;
    }

    /**
     * This method should get unsorted list of Companys from a PartnerService 
     * by iCityId who will check by sCityName in City Mapping, an sort the list
     * 
     * @param string $sCityName
     *
     * @return \Convenia;\Recruiting\Entity\Company[]
     * @throws \InvalidArgumentException
     */
    public function getCompanysForCity($sCityName)
    {
        if (!isset($this->aCityToIdMapping[$sCityName]))
        {
            throw new \InvalidArgumentException(sprintf('Given city name [%s] is not mapped.', $sCityName));
        }
        $this->iCityId = $this->aCityToIdMapping[$sCityName];
        $aSortObject = $this->createSortObject();
        return $aSortObject;
    }

    /**
     * This method orchestra two other methods
     * First we create a possible object of order
     * then sorted in ascending order
     * to finalize recreate the orinal object.
     *
     * @return \Convenia;\Recruiting\Entity\Company[]
     */
    private function createSortObject()
    {
        $this->createSimpleSortableObject();
        asort($this->aSimpleSortable);
        return $this->arrangeObject();
    }
    
    /**
     * To create a simple object that comes from a multidimensional, 
     * we must record the keys for each object and the value of the variable we want.
     * Orignal Archteture (Company) -[hasMany]-> (Partner) -[hasMany]-> (Price)
     * 
     * @return midex[]
     * (hasMany) - [CompanyIdKey],[PartnerIdKey],[PriceIdKey],[PriceAmount]
     */
    private function createSimpleSortableObject()
    {
        $aSimpleSortable = array();
        $aPartnerServiceResult = $this->oPartnerService->getResultForCityId($this->iCityId);
        
        if(!count($aPartnerServiceResult) || !is_array($aPartnerServiceResult))
        {
                throw new \InvalidArgumentException(sprintf('Given argument [%s] as not Array', $aPartnerServiceResult));       
        }
        
        foreach($aPartnerServiceResult as $aCompanyKey => $aCompanyRow)
        {
            if(!$aCompanyRow instanceof Company)
            {
                throw new \InvalidArgumentException(sprintf('Given array [%s] as not instance of Company.', $aCompanyRow));       
            }
            foreach($aCompanyRow->aPartners as $aPartnerKey => $aPartnerRow)
            {
                if(!$aPartnerRow instanceof Partner)
                {
                    throw new \InvalidArgumentException(sprintf('Given array [%s] as not instance of Partner.', $aPartnerRow));       
                }
                foreach($aPartnerRow->aPrices as $aPriceKey => $aPriceRow)
                {
                    if(!$aPriceRow instanceof Price)
                    {
                        throw new \InvalidArgumentException(sprintf('Given array [%s] as not instance of Price.', $aPriceRow));       
                    }
                    $oSortableData = new \StdClass;
                    $oSortableData->Price           = $aPriceRow->fAmount;
                    $oSortableData->PartnerRef      = $aPartnerKey;
                    $oSortableData->PriceRef        = $aPriceKey;
                    $oSortableData->CompanyRef        = $aCompanyKey;
                    $aSimpleSortable[] = $oSortableData;
                }
                
            }
        }    
        
        $this->aSimpleSortable = $aSimpleSortable;
    }

    /**
     * To arrange object from simple sortable object
     * we must order the object by simple sortable object
     * (hasMany) - [CompanyIdKey],[PartnerIdKey],[PriceIdKey],[PriceAmount]
     * 
     * @return \Convenia;\Recruiting\Entity\Company[]
     * (Company) -[hasMany]-> (Partner) -[hasMany]-> (Price)
     */
    private function arrangeObject()
    {
        $aPartnerServiceResult = $this->oPartnerService->getResultForCityId($this->iCityId);
        $aCompanys = array();
        foreach($this->aSimpleSortable as $key => $oSortableRow)
        {
            if(!array_key_exists($oSortableRow->CompanyRef, $aCompanys))
            {
                $oCompany = new Company($aPartnerServiceResult[$oSortableRow->CompanyRef]->sName, $aPartnerServiceResult[$oSortableRow->CompanyRef]->sAdr);
                $aCompanys[$oSortableRow->CompanyRef] = $oCompany;
            }

            if(!array_key_exists($oSortableRow->PartnerRef, $aCompanys[$oSortableRow->CompanyRef]->aPartners))
            {
                $oPartner = new Partner($aPartnerServiceResult[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->sName, $aPartnerServiceResult[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->sHomepage);
                $aCompanys[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef] = $oPartner;
            }
            

            if(!array_key_exists($oSortableRow->PriceRef, $aCompanys[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->aPrices))
            {
                $oPrice = new Price(
                    $aPartnerServiceResult[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->aPrices[$oSortableRow->PriceRef]->sDescription,
                    $aPartnerServiceResult[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->aPrices[$oSortableRow->PriceRef]->fAmount,
                    $aPartnerServiceResult[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->aPrices[$oSortableRow->PriceRef]->oFromDate,
                    $aPartnerServiceResult[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->aPrices[$oSortableRow->PriceRef]->oToDate
                        );
                $aCompanys[$oSortableRow->CompanyRef]->aPartners[$oSortableRow->PartnerRef]->aPrices[$oSortableRow->PriceRef] = $oPrice;
            }
            
        }

        return $aCompanys;

    }
}