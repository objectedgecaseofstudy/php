<?php
namespace Convenia\Recruiting\Directory;

/**
 * Class to work with Directories
 *
 * @author Vinicius Gomes marcusllgomes.com
 */
class Directory 
{
    /**
     * The full Directory.
     *
     * @var string
     */
    private $sDirectory;

    /**
     * Construct new Directory instance.
     *
     * @param string $sDirectory
     */
    public function __construct($sDirectory)
    {
        $this->sDirectory = $this->Validate($sDirectory);    
        
    }

    /**
     * Get the string representation of the Directory Path.
     *
     * @return string
     *   The string representation of the Directory Path.
     */
    public function __toString()
    {
        return (string) $this->sDirectory;
    }

    /**
     * Reset all properties to their defaults.
     *
     * @return $this
     */
    public function Reset()
    {
        $this->sDirectory      = null;
        return $this;
    }

    /**
     * GetDirectory check sDirectoryPath exist, to return full directory path.
     * @param string $sDirectoryPath
     * @return string
     */
    public function Validate($sDirectoryPath)
    {
        $this->Reset();
        if (!file_exists($sDirectoryPath))
        {
            throw new \InvalidArgumentException(sprintf('Given repositorypath [%s] not exist.', $sDirectoryPath));
        }
        
        return $sDirectoryPath;
    }

    /**
     * GetAll return list of stuff inside an repository.
     * @param string $sDirectoryPath
     * @return string
     */
    public function getAll($sDirectoryPath)
    {
        $sDirectory = $this->Validate($sDirectoryPath);   
        $aListThings = array_values(preg_grep('/^([^.])/', scandir($sDirectory)));   
        return $aListThings;

    }

}